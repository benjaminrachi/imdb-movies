import { AbstractControl, ValidationErrors } from "@angular/forms";

export class CustomValidators {

    static isValidYearFormat(control: AbstractControl): ValidationErrors | null {
        let val = control.value;

        const reg = '^[1-2][0-9]{3}$';
        if (!val.toString().match(reg)) return { isValidYearFormat: true };

        return null;
    }

    static isValidUrlFormat(control: AbstractControl): ValidationErrors | null {
        let val = control.value;
        const reg = '(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?';
        if (!val.toString().match(reg)) return { isValidUrlFormat: true };

        return null;
    }

}