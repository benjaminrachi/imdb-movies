import { FullMovieDialogComponent } from './../full-movie-dialog/full-movie-dialog.component';
import { DeleteDialogComponent } from './../delete-dialog/delete-dialog.component';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';

import { Movie } from './../../models/movie';
import { MovieFormComponent } from '../movie-form/movie-form.component';
import { FullMovie } from './../../models/full-movie';
import { MovieService } from './../../services/movie.service';

@Component({
  selector: 'app-movie-card',
  templateUrl: './movie-card.component.html',
  styleUrls: ['./movie-card.component.css']
})
export class MovieCardComponent {
  @Input('movie') movie: Movie;
  @Output("onMovieEdit") onMovieEdit: EventEmitter<Movie> = new EventEmitter();
  @Output("onMovieDeleted") onMovieDeleted: EventEmitter<Movie> = new EventEmitter();
  fullMovie: FullMovie;
  
  constructor(private dialog: MatDialog, private movieService: MovieService) { }


  async openFullInfoDialog() {
    this.fullMovie = await this.movieService.getMovieFullInfo(this.movie.imdbID)


    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.data = this.fullMovie;

    const dialogRef = this.dialog.open(FullMovieDialogComponent, dialogConfig);
  }

  openEditMovieForm() {

    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.data = this.movie;

    const dialogRef = this.dialog.open(MovieFormComponent, dialogConfig);
    dialogRef.afterClosed()
      .subscribe((data) => {
        if (data) {
          //edit mode ==> fire an event
          this.onMovieEdit.emit(data);
        }
      });
  }


  openDeleteDialog() {

    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.data = this.movie;

    const dialogRef = this.dialog.open(DeleteDialogComponent, dialogConfig);
    dialogRef.afterClosed()
      .subscribe((data) => {
        if (data) {
          this.onMovieDeleted.emit(data);
        }
      });
  }
}
