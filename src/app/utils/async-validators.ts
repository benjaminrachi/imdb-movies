import { AbstractControl, ValidationErrors } from "@angular/forms";
import { Injectable } from '@angular/core';
import { MovieService } from './../services/movie.service';
import { Observable } from "rxjs";
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class CustomAsyncValidators {
    constructor(private movieService: MovieService) { }

    checkingTitleUniqueness(control: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> {
        const title = control.value;
        if (!title || title === '') return null;

        return this.movieService.checkingTitleUniqueness(title)
            .pipe(
                map((movieFound) => {
                    const exists = movieFound && movieFound.Title && (movieFound.Title.toLowerCase() === title.toLowerCase());
                    return exists ? { titleAlreadyExists: true } : null;
                })
            )
    }
} 