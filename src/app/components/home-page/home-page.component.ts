import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { MovieFormComponent } from './../movie-form/movie-form.component';
import { MovieService } from './../../services/movie.service';
import { Movie } from './../../models/movie';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  movies: Movie[] = [];
  pageIndex = 1;
  title = 'star';

  constructor(private movieService: MovieService, private dialog: MatDialog) { }

  ngOnInit() {
    this.movieService.searchMovies(this.title, this.pageIndex)
      .subscribe((data) => {
        this.movies = data.Search || [];
      })

  }
  searchMovie(query: HTMLInputElement) {
    this.title = query.value;
    query.value = '';
    this.pageIndex = 1;
    this.movieService.searchMovies(this.title, this.pageIndex)
      .subscribe((data) => {
        this.movies = data.Search || [];
      })
  }

  isPrevDisabled() {
    return (this.pageIndex === 1);
  }

  goPrev() {
    if (this.isPrevDisabled()) return;

    this.pageIndex--;
    this.movieService.searchMovies(this.title, this.pageIndex)
      .subscribe((data) => {
        this.movies = data.Search || [];
      })
  }

  isNextDisabled() {
    return (this.pageIndex === 100 || this.movies.length === 0);
  }

  goNext() {
    if (this.isNextDisabled()) return;

    this.pageIndex++;
    this.movieService.searchMovies(this.title, this.pageIndex)
      .subscribe((data) => {
        this.movies = data.Search || [];
      })
  }
  openAddMovieForm() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.data = null;

    const dialogRef = this.dialog.open(MovieFormComponent, dialogConfig);
    dialogRef.afterClosed().subscribe((data) => {
      if (data) { this.movies.push(data); }
    });
  }

  movieEditHandler(movie: Movie) {
    const movieIndex = this.movies.findIndex(m => m.imdbID === movie.imdbID);
    this.movies[movieIndex] = movie;
  }

  movieDeletedHandler(movie: Movie) {
    const movieIndex = this.movies.findIndex(m => m.imdbID === movie.imdbID);
    this.movies.splice(movieIndex, 1);
  }

}
