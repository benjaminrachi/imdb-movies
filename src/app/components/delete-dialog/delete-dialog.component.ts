import { Component, Inject } from '@angular/core';
import { Movie } from 'src/app/models/movie';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-delete-dialog',
  templateUrl: './delete-dialog.component.html',
  styleUrls: ['./delete-dialog.component.css']
})
export class DeleteDialogComponent {
  movie: Movie;
  dialogTitle = "Delete a movie";

  constructor(
    private dialogRef: MatDialogRef<DeleteDialogComponent>,
    @Inject(MAT_DIALOG_DATA) data) {
    this.movie = data;

    if (this.movie) {
      this.dialogTitle = `Delete ${this.movie.Title}`;
    }
  }

  close() {
    this.dialogRef.close();
  }

  delete() {
    this.dialogRef.close(this.movie);
  }
}
