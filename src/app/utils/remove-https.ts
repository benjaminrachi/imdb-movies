import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: 'removeHttp' })
export class RemoveHttpPipe implements PipeTransform {

    transform(input: string) {
        return input.replace(/^http?:/, '');
    }

}