import { MovieCardComponent } from './components/movie-card/movie-card.component';
import { RouterModule, Routes } from '@angular/router';

import { HomePageComponent } from './components/home-page/home-page.component';
import { MovieFormComponent } from './components/movie-form/movie-form.component';

export const routes: Routes = [
    { path: '', component: HomePageComponent },
    { path: 'movie/:id', component: MovieCardComponent },
    { path: 'movie-form', component: MovieFormComponent },
];