import { FullMovie } from './../../models/full-movie';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-full-movie-dialog',
  templateUrl: './full-movie-dialog.component.html',
  styleUrls: ['./full-movie-dialog.component.css']
})
export class FullMovieDialogComponent {
  fullMovie: FullMovie;

  constructor(private dialogRef: MatDialogRef<FullMovieDialogComponent>,
    @Inject(MAT_DIALOG_DATA) data) {
    this.fullMovie = data;
  }

  close() {
    this.dialogRef.close();
  }

  delete() {
    this.dialogRef.close(this.fullMovie);
  }

}





