import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FullMovieDialogComponent } from './full-movie-dialog.component';

describe('FullMovieDialogComponent', () => {
  let component: FullMovieDialogComponent;
  let fixture: ComponentFixture<FullMovieDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FullMovieDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FullMovieDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
