import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { routes } from './routes';
import { AppComponent } from './app.component';
import { HomePageComponent } from './components/home-page/home-page.component';
import { MovieCardComponent } from './components/movie-card/movie-card.component';
import { MovieFormComponent } from './components/movie-form/movie-form.component';
import { DeleteDialogComponent } from './components/delete-dialog/delete-dialog.component';
import { FullMovieDialogComponent } from './components/full-movie-dialog/full-movie-dialog.component';
import { NonEnglishPipe } from './utils/non-letters-pipe';
import { RemoveHttpPipe } from './utils/remove-https';

import {
  MatCardModule, MatButtonModule,
  MatFormFieldModule, MatInputModule,
  MatDialogModule, MatSelectModule, MatIconModule
} from '@angular/material';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    MovieCardComponent,
    MovieFormComponent,
    DeleteDialogComponent,
    NonEnglishPipe,
    RemoveHttpPipe,
    FullMovieDialogComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MatCardModule,
    MatButtonModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatIconModule,
    RouterModule.forRoot(routes),
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [MovieFormComponent, DeleteDialogComponent, FullMovieDialogComponent]
})
export class AppModule { }
