import { FullMovie } from './../models/full-movie';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MovieService {
  baseUrl = 'http://www.omdbapi.com/?apikey=798a97fc';

  constructor(private http: HttpClient) { }

  searchMovies(query: string, pageIndex: number): any {
    const options ={params: new HttpParams()
        .set('s', query || 'star')
        .set('page', pageIndex.toString() || '1')
    };

    return this.http.get(this.baseUrl, options);
  }

  getMovieFullInfo(id: string) {
    const options ={params: new HttpParams().set('i', id)};

    return this.http.get<FullMovie>(this.baseUrl, options).toPromise();
  }

  checkingTitleUniqueness(title: string) {
    return this.http.get<FullMovie>(`${this.baseUrl}&t=${title}`);
  }
}
