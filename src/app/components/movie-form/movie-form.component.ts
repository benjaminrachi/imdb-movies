import { CustomAsyncValidators } from './../../utils/async-validators';
import { CustomValidators } from './../../utils/validators';
import { Movie } from 'src/app/models/movie';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-movie-form',
  templateUrl: './movie-form.component.html',
  styleUrls: ['./movie-form.component.css']
})
export class MovieFormComponent implements OnInit{

  movie: Movie;
  submitted = false;
  formTitle = "Add a movie";
  movieForm: FormGroup;

  constructor(
    private dialogRef: MatDialogRef<MovieFormComponent>,
    @Inject(MAT_DIALOG_DATA) data,
    private formBuilder: FormBuilder,
    private asyncValidators: CustomAsyncValidators) {
    
    this.movie = data;
  }

  ngOnInit() {
    this.movieForm = this.formBuilder.group({
      Title: ['', Validators.required,
        this.asyncValidators.checkingTitleUniqueness.bind(this.asyncValidators)
      ],
      Year: ['', [Validators.required, CustomValidators.isValidYearFormat]],
      imdbID: [{ value: null }],
      Poster: ['', [Validators.required, CustomValidators.isValidUrlFormat]]
    });

    if (this.movie) {
      this.formTitle = `Edit ${this.movie.Title}`;
      this.initializeForm();

    }
}
  get title() { return this.movieForm.get('Title'); }

  get year() { return this.movieForm.get('Year'); }

  get poster() { return this.movieForm.get('Poster'); }



  initializeForm() {
    this.movieForm.setValue({
      imdbID: this.movie.imdbID,
      Title: this.movie.Title,
      Year: this.movie.Year,
      Poster: this.movie.Poster
    })
  }

  close() {
    this.dialogRef.close();
  }


  onSubmit() {
    this.submitted = true;
    console.log('movie form', this.movieForm);
    // stop here if form is invalid
    if (this.movieForm.invalid) {
      return;
    }

    this.dialogRef.close(this.movieForm.value);
  }
}
