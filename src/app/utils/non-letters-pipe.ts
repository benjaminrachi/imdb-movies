import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: 'validEnglish' })
export class NonEnglishPipe implements PipeTransform {

    transform(input: string) {
        return input.replace(/[^a-zA-Z0-9 ]/g, '');
    }

}